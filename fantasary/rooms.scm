;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (fantasary rooms)
  #:use-module (goblins)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib let-on)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib pubsub)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (^sink
            ^pk
            ^identity
            ^profile
            ^entity
            ^puppet
            ^room
            ^controller
            ^petname-registry
            ^hub))

;; Accept anything and do nothing.
(define (^sink _bcom) (lambda _ #f))

;; Accept anything and pk it!
(define (^pk _bcom) pk)

;; User identity information.
(define (^identity _bcom init-name)
  (define name (spawn ^cell init-name))
  (methods
   ((self-proposed-name) ($ name))
   ((set-name new-name) ($ name new-name))))

;; A read-only identity.  This is what we can freely share for others
;; to associate with a petname.
(define (^profile _bcom identity)
  (methods
   ((self-proposed-name) ($ identity 'self-proposed-name))))

;; An object in the world.
(define* (^entity _bcom profile #:optional (init-event-handler (spawn ^sink)))
  (define event-handler (spawn ^cell init-event-handler))
  (methods
   ;; Get the read-only identity.
   ((profile) profile)
   ;; For convenience:
   ((self-proposed-name)
    ($ profile 'self-proposed-name))
   ;; Event handling:
   ((notify event)
    (<-np ($ event-handler) event))
   ((set-event-handler new-event-handler)
    ($ event-handler new-event-handler))))

;; A proxy to an entity (or another puppet!) with a revokable
;; connection.
(define* (^puppet _bcom entity #:key (event-wrapper identity))
  (define connected? (spawn ^cell #t))
  (methods
   ((profile)
    (<- entity 'profile))
   ((self-proposed-name)
    (<- entity 'self-proposed-name))
   ((notify event)
    ;; Interaction is not possible once the connection is severed.
    (when ($ connected?)
      (<-np entity 'notify (event-wrapper event))))
   ((cut!)
    ($ connected? #f))))

;; A shared space in the game world containing zero or more entities.
(define (^room _bcom profile)
  (define entities (spawn ^cell '()))
  (define exits (spawn ^cell '()))
  ;; A lookup table for puppets.  Maps (from, to) tuples to puppets.
  ;; Every entity gets their own set of puppets for every other entity
  ;; in the room.  This is implemented with an alist for simplicity,
  ;; which comes with the hopefully obvious caveat that we'd need
  ;; something better if we wanted this to scale up well.
  (define puppets (spawn ^cell '()))
  ;; Indexes.
  (define profiles->entities (spawn ^cell '()))
  (define profiles->exits (spawn ^cell '()))
  ;; Make a puppet for an entity in the room specific to a particular
  ;; client.  All events are wrapped to indicate that this is a direct
  ;; message from another entity.
  (define (spawn-client-puppet from to)
    (spawn ^puppet from
           #:event-wrapper
           (lambda (event)
             `(direct ,to ,@event))))
  (define (find-puppet from to)
    (match (find (match-lambda
                   (((from* . to*) . puppet)
                    (and (eq? from from*)
                         (eq? to to*))))
                 ($ puppets))
      ((_ . puppet) puppet)
      (_ #f)))
  ;; Helper to lazily generate puppets for clients.
  (define (puppet-ref from to)
    (or (find-puppet from to)
        (let ((new-puppet (spawn-client-puppet from to)))
          ($ puppets (alist-cons (cons from to) new-puppet ($ puppets)))
          new-puppet)))
  ;; Helper to send event to all entities in the room.
  (define (broadcast subject type . args)
    (for-each (lambda (to)
                (<-np to 'notify (cons* type (puppet-ref subject to) args)))
              ($ entities)))
  ;; Remove entity from room.
  (define (remove-entity entity)
    ($ entities (delq entity ($ entities)))
    ;; Invalidate all puppets associated with the removed entity.
    ($ puppets (filter (match-lambda
                         (((from . to) . puppet)
                          (if (or (eq? from entity) (eq? to entity))
                              (begin
                                ($ puppet 'cut!)
                                #f)
                              #t)))
                       ($ puppets)))
    ;; Remove from indexes.
    ($ profiles->entities
       (remove (match-lambda
                 ((_ . e)
                  (eq? e entity)))
               ($ profiles->entities))))
  ;; Per-entity client.
  (define (^room-client _bcom entity)
    (methods
     ((exit)
      (broadcast entity 'exit)
      (remove-entity entity))
     ((room-profile)
      profile)
     ((room-name)
      ($ profile 'self-proposed-name))
     ((look)
      (define other-entities
        (filter-map (lambda (other)
                      (if (eq? other entity)
                          #f ; don't look at yourself
                          (puppet-ref other entity)))
                    ($ entities)))
      `((entities . ,other-entities)
        (exits . ,($ exits))))
     ((say message)
      (broadcast entity 'say message))
     ((emote message)
      (broadcast entity 'emote message))
     ((lookup-by-profile profile)
      (define other-entity (assq-ref ($ profiles->entities) profile))
      (and other-entity
           (puppet-ref other-entity entity)))
     ((lookup-exit-by-profile profile)
      (assq-ref ($ profiles->exits) profile))))
  ;; General room methods.
  (methods
   ((profile) profile)
   ((self-proposed-name)
    ($ profile 'self-proposed-name))
   ((add-entity entity)
    ;; Don't allow the same object to be inserted more than once.
    (when (memq entity ($ entities))
      (throw 'duplicate-entity entity))
    ;; Notify room.
    (broadcast entity 'enter)
    (let*-on ((profile (<- entity 'profile))
              (name (<- profile 'self-proposed-name)))
      ;; Add to entities list.
      ($ entities (cons entity ($ entities)))
      ;; Add to index.
      ($ profiles->entities (alist-cons profile entity ($ profiles->entities)))
      ;; Return a client object.
      (spawn ^room-client entity)))
   ((remove-entity entity)
    (remove-entity entity))
   ((add-exit room)
    (when (memq room ($ exits))
      (throw 'duplicate-exit room))
    ($ exits (cons room ($ exits)))
    ;; Add to index.
    ($ profiles->exits (alist-cons ($ room 'profile) room ($ profiles->exits))))
   ((remove-exit room)
    ($ exits (delq ($ exits) room))
    ;; Delete from indexes.
    ($ profiles->exits
       (remove (match-lambda
                 ((_ . e)
                  (eq? e room)))
               ($ profiles->exits))))))

;; A central actor for clients to use to create an identity and join
;; the game world.
(define (^hub _bcom spawn-point)
  (methods
   ((make-identity name)
    (spawn ^identity name))
   ((make-profile identity)
    (spawn ^profile identity))
   ((make-entity profile)
    (spawn ^entity profile))
   ((make-puppet entity)
    (spawn ^puppet entity))
   ((spawn-point) spawn-point)))

;; A client scoped to a current room.
(define (^controller _bcom hub entity)
  (define current-room (spawn ^cell #f))
  (define current-puppet (spawn ^cell #f))
  (define current-client (spawn ^cell #f))
  (define (check-client)
    (unless ($ current-client)
      (throw 'not-in-a-room)))
  (define (exit-room-maybe)
    (let ((client ($ current-client)))
      (when client
        ;; Invalidate puppet for the old room.
        (let ((puppet ($ current-puppet)))
          (<-np puppet 'cut!))
        ($ current-room #f)
        ($ current-client #f)
        (<- client 'exit))))
  (methods
   ((enter new-room)
    (define old-room ($ current-room))
    (define old-client ($ current-client))
    (when (eq? new-room old-room)
      (throw 'same-room new-room))
    ;; If already in a room, leave it.
    (exit-room-maybe)
    ;; Create a new puppet for the room.
    (let-on ((new-puppet (<- hub 'make-puppet entity)))
      ;; Join the new room.
      (define new-client (<- new-room 'add-entity new-puppet))
      ($ current-puppet new-puppet)
      ($ current-room new-room)
      ($ current-client new-client)))
   ((quit)
    (exit-room-maybe))
   ((room-profile)
    (check-client)
    (<- ($ current-client) 'room-profile))
   ((room-name)
    (check-client)
    (<- ($ current-client) 'room-name))
   ((lookup-by-profile profile)
    (check-client)
    (<- ($ current-client) 'lookup-by-profile profile))
   ((lookup-exit-by-profile profile)
    (check-client)
    (<- ($ current-client) 'lookup-exit-by-profile profile))
   ((look)
    (check-client)
    (<- ($ current-client) 'look))
   ((say message)
    (check-client)
    (<-np ($ current-client) 'say message))
   ((emote message)
    (check-client)
    (<-np ($ current-client) 'emote message))))

;; A database mapping profiles to petnames.
(define (^petname-registry _bcom)
  (define spns->profiles (spawn ^cell '()))
  (define profiles->spns (spawn ^cell '()))
  (define petnames->profiles (spawn ^cell '()))
  (define profiles->petnames (spawn ^cell '()))
  (define (spn-occurrences spn)
    (fold (lambda (item count)
            (match item
              (((spn* . _) . _)
               (if (string=? spn spn*)
                   (+ count 1)
                   count))
              (_ count)))
          0 ($ spns->profiles)))
  (define (spn-ref profile)
    (let loop ((spns ($ spns->profiles)))
      (match spns
        (() #f)
        (((spn . profile*) . rest)
         (if (eq? profile profile*)
             spn
             (loop rest))))))
  (methods
   ((lookup-profile-for-petname petname)
    (assoc-ref ($ petnames->profiles) petname))
   ((lookup-profile-for-self-proposed-name spn index)
    (assoc-ref ($ spns->profiles) (cons spn index)))
   ((lookup-self-proposed-name profile)
    (assq-ref ($ profiles->spns) profile))
   ((lookup-petname profile)
    (assq-ref ($ profiles->petnames) profile))
   ((register-self-proposed-name spn profile)
    (or (spn-ref profile)
        (let* ((index (spn-occurrences spn))
               (spn* (cons spn index)))
          ($ spns->profiles (alist-cons spn* profile ($ spns->profiles)))
          ($ profiles->spns (alist-cons profile spn* ($ profiles->spns)))
          spn*)))
   ((register-petname petname profile)
    ($ petnames->profiles (alist-cons petname profile ($ petnames->profiles)))
    ($ profiles->petnames (alist-cons profile petname ($ profiles->petnames))))))
