;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (fantasary client)
  #:use-module (fantasary concurrent-queue)
  #:use-module (fantasary config)
  #:use-module (fantasary event-loop)
  #:use-module (fantasary ncurses-vat)
  #:use-module (fantasary ncurses-stuff)
  #:use-module (fantasary rooms)
  #:use-module (fantasary tls-cert)
  #:use-module (fibers conditions)
  #:use-module (fibers operations)
  #:use-module (fibers timers)
  #:use-module (goblins)
  #:use-module (goblins actor-lib joiners)
  #:use-module (goblins actor-lib let-on)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer tcp-tls)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ncurses curses)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (system repl coop-server)
  #:declarative? #f
  #:export (launch-client))

(define %BACKSPACE 263)
(define tls-key-file-name "client.key")
(define tls-cert-file-name "client.cert")
(define tasks (make-concurrent-queue))
(define screen (screen-setup!))
(define machine-vat #f)
(define user-vat #f)
(define ui-vat #f)
(define prompt-input '())
;; Stateful things that need to be setup once the event loop has
;; booted.
(define client-name #f)
(define client-sturdyref-file #f)
(define tcp-netlayer #f)
(define mycapn #f)
(define hub-vow #f)
(define identity-vow #f)
(define profile-vow #f)
(define petnames #f)
(define entity-vow #f)
(define controller #f)
(define logger #f)
(define title-win #f)
(define log-win #f)
(define prompt-win #f)

(define-record-type <self-proposed-name>
  (%self-proposed-name name index)
  self-proposed-name?
  (name self-proposed-name-ref)
  (index self-proposed-name-index))

(define* (self-proposed-name name #:optional (index 0))
  (%self-proposed-name name index))

(define (string-every pred str)
  (let loop ((i 0))
    (if (< i (string-length str))
        (let ((char (string-ref str i)))
          (and (pred char) (loop (+ i 1))))
        #t)))

(define (escape-name name)
  (if (string-every alphanumeric? name)
      name
      (list->string
       `(#\{
         ,@(let loop ((i 0))
             (if (< i (string-length name))
                 (match (string-ref name i)
                   (#\{
                    (cons* #\\ #\{ (loop (+ i 1))))
                   (#\}
                    (cons* #\\ #\} (loop (+ i 1))))
                   (char
                    (cons char (loop (+ i 1)))))
                 '()))
         #\}))))

(define (print-self-proposed-name name port)
  (let ((i (self-proposed-name-index name))
        (name* (escape-name
                (self-proposed-name-ref name))))
    (if (= i 0)
        (format port "?~a" name*)
        (format port "?~a+~a" name* i))))

(set-record-type-printer! <self-proposed-name> print-self-proposed-name)

(define-record-type <petname>
  (petname name)
  petname?
  (name petname-ref))

(define (print-petname name port)
  (format port "@~a" (escape-name (petname-ref name))))

(set-record-type-printer! <petname> print-petname)

(define (name? obj)
  (or (self-proposed-name? obj)
      (petname? obj)))

(define (name->string name)
  (format #f "~a" name))

(define-syntax-rule (trampoline proc args ...)
  (lambda (args ...)
    (proc args ...)))

(define (collect pred exp)
  (match exp
    ((exps ...)
     (delete-duplicates
      (concatenate
       (map (lambda (exp*)
              (collect pred exp*))
            exps))))
    (x
     (if (pred x) (list x) '()))))

(define (all-of*-maybe refrs)
  (match refrs
    (()
     (let ()
       (define-values (promise resolver)
         (spawn-promise-values))
       ($ resolver 'fulfill '())
       promise))
    (_
     (all-of* refrs))))

;; Loggers
(define (^logger:stdout _bcom)
  (lambda (msg)
    (display msg)
    (newline)))

(define (addchstr* win chstr)
  "Add CHSTR to WIN and move cursor."
  (addchstr win chstr)
  (move win (getcury win)
        (+ (getcurx win) (length chstr))))

(define (log-to-curses win msg)
  (match msg
    ((? string?)
     (addch win (color %RED-N (bold #\%)))
     (addstr win (format #f " ~a\n" msg)))
    (('*error* e)
     (addchstr* win (color %RED-N (bold "[ERROR]")))
     (addchstr* win (color %RED-N (format #f " ~a" e)))
     (addstr win "\n"))
    (('*enter* name)
     (addchstr* win (color %WHITE-N (bold "=> ")))
     (display-name win name)
     (addstr win " enters the room.\n"))
    (('*exit* name)
     (addchstr* win (color %WHITE-N (bold "<= ")))
     (display-name win name)
     (addstr win " exits the room.\n"))
    (('*say* name said)
     (addch win (color %WHITE-N (bold #\<)))
     (display-name win name)
     (addch win (color %WHITE-N (bold #\>)))
     (addstr win " ")
     (display-phrase win said)
     (addstr win "\n"))
    (('*emote* name said)
     (addch win (color %WHITE-N (bold #\*)))
     (addstr win " ")
     (display-name win name)
     (addstr win " ")
     (display-phrase win said)
     (addstr win "\n")))
  (refresh win)
  (refresh prompt-win))      ; set cursor back to prompt

(define (^logger:curses _bcom win)
  (lambda (msg)
    (log-to-curses win msg)))


;; Display procedures for ncurses

(define (display-name win spn-or-petname)
  (if (self-proposed-name? spn-or-petname)
      (display-self-proposed-name win spn-or-petname)
      (display-petname win spn-or-petname)))

(define (display-self-proposed-name win self-proposed-name)
  (define name (self-proposed-name-ref self-proposed-name))
  (define index (self-proposed-name-index self-proposed-name))
  (addch win (color %RED-N (bold #\?)))
  (addchstr* win (color %RED-N name))
  (unless (= index 0)
    (addchstr* win (color %RED-N (format #f "+~a" index)))))

(define (display-petname win petname)
  (define name (petname-ref petname))
  (addch win (color %YELLOW-N (bold #\@)))
  (addchstr* win (color %YELLOW-N name)))

(define (display-phrase win phrase)
  (match phrase
    ((? string? str)
     (addstr win str))
    ((? number? num)
     (addstr win (number->string num)))
    ((? self-proposed-name? spn)
     (display-self-proposed-name win spn))
    ((? petname? pn)
     (display-petname win pn))
    ((phrases ...)
     (for-each (lambda (phrase*)
                 (display-phrase win phrase*))
               phrases))
    (other
     (addstr win (format #f "~s" other)))))


;; Log helpers
(define (log str)
  (<-np logger str))

(define (echo str)
  (log (string-append "> " str)))

(define (log-pk . args)
  (log (format #f "~s" args))
  (match args
    (() '())
    ((_ ... last) last)))

(define (log-error e)
  (log `(*error* ,e)))

;; Various name lookup helpers
(define (lookup-name-for-profile profile)
  (let*-on ((spn (<- profile 'self-proposed-name))
            (spn+index (<- petnames 'register-self-proposed-name spn profile)))
    (let-on ((pet (<- petnames 'lookup-petname profile)))
      (cond
       (pet
        (petname pet))
       (else
        (match spn+index
          ((name . index)
           (self-proposed-name name index))))))))

(define (lookup-name-for-entity entity)
   (let-on ((profile (<- entity 'profile)))
     (lookup-name-for-profile profile)))

(define (lookup-name-for-room room)
   (let-on ((profile (<- room 'profile)))
     (lookup-name-for-profile profile)))

(define (resolve-name name)
  (cond
   ((self-proposed-name? name)
    (let ((name* (self-proposed-name-ref name))
          (index (self-proposed-name-index name)))
      (<- petnames 'lookup-profile-for-self-proposed-name name* index)))
   ((petname? name)
    (<- petnames 'lookup-profile-for-petname (petname-ref name)))
   ;; Shouldn't get here but just in case...
   (else
    (let ()
      (define-values (promise resolver)
        (spawn-promise-values))
      ($ resolver 'fulfill #f)
      promise))))

;; Return a promise that transforms exp into a new exp where all name
;; references have been replaced with the resolved profiles for those
;; names.  The promise breaks if a name cannot be resolved.
(define (resolve-names exp)
  (define names (collect name? exp))
  (define profile-vows (map resolve-name names))
  (let-on ((profiles (all-of*-maybe profile-vows)))
    (let ((table (map cons names profiles)))
      (let loop ((exp exp))
        (match exp
          ((? name? name)
           (assq-ref table name))
          ((exps ...)
           (map loop exps))
          (_ exp))))))

;; Like above, but reversed.  Transform profiles into name references.
(define (resolve-profiles exp)
  (define profiles (collect live-refr? exp))
  (define name-vows (map (lambda (profile)
                       (lookup-name-for-profile profile))
                     profiles))
  (let-on ((names (all-of*-maybe name-vows)))
    (let ((table (map cons profiles names)))
      (let loop ((exp exp))
        (match exp
          ((? live-refr? refr)
           (assq-ref table refr))
          ((exps ...)
           (map loop exps))
          (_ exp))))))

;; Transform a message expression containing strings, numbers, and
;; names into a human readable string for printing.
(define (render-message message)
  (match message
    ((? string?)
     message)
    ((? number? n)
     (number->string n))
    ((? name? name)
     (name->string name))
    ((items ...)
     (string-concatenate
      (map render-message items)))
    (exp
     (format #f "~a" exp))))

;; Event handlers
(define (^event-handler:echo _bcom logger)
  (lambda (event)
    (<-np logger (format #f "~s" event))))

(define (^event-handler:fancy _bcom logger)
  (match-lambda
    (('enter who)
     (on (lookup-name-for-entity who)
         (lambda (name)
           (log `(*enter* ,name)))))
    (('exit who)
     (on (lookup-name-for-entity who)
         (lambda (name)
           (log `(*exit* ,name)))))
    (('say who message)
     (let-on ((name (lookup-name-for-entity who))
              (message* (resolve-profiles message)))
       (log `(*say* ,name ,message*))))
    (('emote who message)
     (let-on ((name (lookup-name-for-entity who))
              (message* (resolve-profiles message)))
       (log `(*emote* ,name ,message*))))
    ;; Unknown event
    (event
     (log (format #f "Received unknown event: ~s" event)))))

;; Actions
(define (say message)
  (on (resolve-names message)
      (lambda (message*)
        (<-np controller 'say message*))
      #:catch log-error))

(define (emote message)
  (on (resolve-names message)
      (lambda (message*)
        (<-np controller 'emote message*))
      #:catch log-error))

(define (look)
  (let-on ((contents (<- controller 'look))
           (room-profile (<- controller 'room-profile)))
    (let* ((entities (assq-ref contents 'entities))
           (exits (assq-ref contents 'exits))
           (entity-names-vow
            (all-of*-maybe
             (map (lambda (e)
                    (lookup-name-for-entity e))
                  entities)))
           (exit-names-vow
            (all-of*-maybe
             (map (lambda (room)
                    (lookup-name-for-room room))
                  exits))))
      (let-on ((room-name (lookup-name-for-profile room-profile))
               (entity-names entity-names-vow)
               (exit-names exit-names-vow))
        (log (format #f "You are in ~a." room-name))
        (match entity-names
          (()
           (log "There's nothing else in this room.  Just you."))
          (_
           (log "You look around the room and see:")
           (for-each (lambda (name)
                       (log (format #f "- ~a" name)))
                     entity-names)))
        (match exit-names
          (()
           (log "Uh oh, there are no exits!"))
          (_
           (log "You see some exits:")
           (for-each (lambda (name)
                       (log (format #f "- ~a" name)))
                     exit-names)))))))

(define (go exit-name)
  (let-on ((profile (resolve-name exit-name)))
    (if (live-refr? profile)
        (let-on ((room (<- controller 'lookup-exit-by-profile profile)))
          (if room
              (on (<- controller 'enter room)
                  (lambda _
                    (add-task! tasks refresh-title)
                    (look)))
              (log (format #f "No such exit: ~a" exit-name))))
        (log (format #f "No such exit: ~a" exit-name)))))

(define (run target-name args)
  (let-on ((profile (resolve-name target-name)))
    (if (live-refr? profile)
        (let-on ((entity (<- controller 'lookup-by-profile profile)))
          (if entity
              (<-np entity 'notify args)
              (log (format #f "No such user: ~a" target-name))))
        (log (format #f "No such user: ~a" target-name)))))

(define (register-petname old new)
  (let-on ((profile (resolve-name old)))
    (if (live-refr? profile)
        (let ((new-name (petname-ref new)))
          ($ petnames 'register-petname new-name profile)
          (log (format #f "Registered petname ~a for ~a" new old))
          (add-task! tasks refresh-title))
        (log (format #f "No object with name: ~a" old)))))

(define (help)
  (log "")
  (log "HELP:")
  (log "To say something to the room, just type some text and hit ENTER.")
  (log "To run a command, prefix it with / and then specify the arguments.")
  (log "")
  (log "COMMANDS:")
  (log "/look: Describe what is in the current room.")
  (log "/name NAME PETNAME : Register PETNAME for the object NAME.")
  (log "/me MESSAGE: Emote a MESSAGE")
  (log "/go EXIT: Go through EXIT to a new room.")
  (log "/run TARGET ARGS ...: Interact with object TARGET by sending ARGS to it.")
  (log "")
  (log "NAMES:")
  (log "Names beginning with ? are the names given to objects by their operator.")
  (log "Names beginning with @ are petnames that you have given to certain objects.")
  (log "Any name you see beginning with ? can be given a petname.")
  (log "Use the /name command to give petnames to objects.")
  (log "")
  (log "Type /help to see this information again.")
  (log ""))

(define (whitespace? char)
  (char-set-contains? char-set:whitespace char))

(define (name-prefix? char)
  (or (eqv? char #\?)
      (eqv? char #\@)))

(define (alphanumeric? char)
  (char-set-contains? char-set:letter+digit char))

(define (digit? char)
  (char-set-contains? char-set:digit char))

(define (parse-name port)
  (define type
    (match (read-char port)
      (#\? 'spn)
      (#\@ 'petname)))
  (define name
    (if (eqv? (peek-char port)  #\{)
        ;; ?{Names With Spaces and Stuff} syntax.  Supports \ escaping
        ;; so that ?{foo\{\}} is a valid name.
        (begin
          (read-char port)
          (list->string
           (let loop ()
             (match (peek-char port)
               ((? eof-object?) '())
               (#\}
                (read-char port)
                '())
               (#\\
                (read-char port)
                (cons (read-char port) (loop)))
               (_
                (cons (read-char port) (loop)))))))
        ;; No curly braces means simple parsing of alphanumeric
        ;; characters only.
        (list->string
         (let loop ()
           (match (peek-char port)
             ((? eof-object?) '())
             ((? alphanumeric?)
              (cons (read-char port) (loop)))
             (_ '()))))))
  (define index
    (if (and (eq? type 'spn) (eqv? (peek-char port) #\+))
        (begin
          (read-char port)
          (string->number
           (list->string
            (let loop ()
              (match (peek-char port)
                ((? eof-object?) '())
                ((? digit?)
                 (cons (read-char port) (loop)))
                (_ '()))))))
        0))
  (case type
    ((spn)
     (self-proposed-name name index))
    ((petname)
     (petname name))))

;; Parse user input into a s-expression that we can process.
(define (parse-command port)
  (define (consume-whitespace)
    (match (peek-char port)
      ((? eof-object?) *unspecified*)
      ((? whitespace?)
       (read-char port)
       (consume-whitespace))
      (_ *unspecified*)))
  (define (parse-verb)
    (match (peek-char port)
      (#\/
       (read-char port)
       (string->symbol
        (list->string
         (let loop ()
           (match (read-char port)
             ((or (? eof-object?) (? whitespace?))
              '())
             (char
              (cons char (loop))))))))
      (_ 'say)))
  (define (parse-plain-text)
    (list->string
     (let loop ((prev-whitespace? #f))
       (match (peek-char port)
         ((? eof-object?)
          '())
         ((? whitespace? char)
          (read-char port)
          (cons char (loop #t)))
         ((and (? name-prefix?) char)
          (if prev-whitespace?
              '()
              (begin
                (read-char port)
                (cons char (loop #f)))))
         (char
          (read-char port)
          (cons char (loop #f)))))))
  (define (parse-markup)
    (match (peek-char port)
      ((? eof-object?)
       '())
      ((? name-prefix?)
       (cons (parse-name port) (parse-markup)))
      (_
       (cons (parse-plain-text) (parse-markup)))))
  (define (parse-run-arg arg)
    (match arg
      ((or ('self-proposed-name _) ('petname _))
       (list arg))
      (_
       (call-with-input-string arg
         (lambda (port*)
           (let loop ()
             (match (read port*)
               ((? eof-object?)
                '())
               (exp
                (cons exp (loop))))))))))
  (define (parse-scheme)
    (consume-whitespace)
    (match (peek-char port)
      ((? eof-object?)
       '())
      ((? name-prefix?)
       (cons (parse-name port)
             (parse-scheme)))
      (_
       (cons (read port) (parse-scheme)))))
  (define (expand-names exp)
    (match exp
      ((? symbol?)
       (let ((str (symbol->string exp)))
         (if (memv (string-ref str 0) '(#\? #\@))
             (call-with-input-string str parse-name)
             exp)))
      ((exps ...)
       (map expand-names exps))
      (_ exp)))
  (consume-whitespace)
  (define verb (parse-verb))
  (define args
    (if (memq verb '(say me))
        (list (parse-markup))
        (expand-names (parse-scheme))))
  (cons verb args))

(define (refresh-prompt)
  (define size (getmaxyx screen))
  (define width (list-ref size 1))
  (define prompt-str (list->string (reverse prompt-input)))
  (define prompt-str-len (string-length prompt-str))
  (define max-prompt-to-show (- width 4))
  (clear prompt-win)
  (hline prompt-win (acs-hline) width #:x 0 #:y 0)
  (addch prompt-win
         (color %YELLOW-N (bold #\>))
         #:x 1 #:y 1)
  (addstr prompt-win
          ;; cut off the length of the string shown, if the user has typed a lot
          (if (> prompt-str-len max-prompt-to-show)
              (let ((str-start (- prompt-str-len max-prompt-to-show)))
                (substring prompt-str str-start))
              prompt-str)
          #:x 3 #:y 1)
  (move prompt-win 1 (+ 3 (length prompt-input)))
  (refresh prompt-win))

(define (set-title title)
  (define size (getmaxyx screen))
  (define width (list-ref size 1))
  (clear title-win)
  (addstr title-win title #:x 0 #:y 0)
  (hline title-win (acs-hline) width #:x 0 #:y 1)
  (refresh title-win)
  (refresh prompt-win))    ; set cursor back to prompt

(define (refresh-title)
  (define (on-error e)
    (log-pk e)
    (set-title "Error: could not get room name"))
  (with-vat ui-vat
    (on (<- controller 'room-profile)
        (lambda (profile)
          (on (lookup-name-for-profile profile)
              (lambda (name)
                (set-title (format #f "Room: ~a" name)))
              #:catch on-error))
        #:catch on-error)))

(define (init screen)
  ;; Spawn vats.  Send their output (notably backtraces when errors
  ;; happen) to the void because it ends up being a mess when it mixes
  ;; with curses.
  (parameterize ((current-output-port (%make-void-port "w")))
    (set! machine-vat (spawn-vat #:name "OCapN" #:log? #t))
    (set! user-vat (spawn-vat #:name "User" #:log? #t))
    (set! ui-vat (spawn-ncurses-vat tasks #:name "UI" #:log? #t)))
  ;; Setup ncurses windows
  (define size (getmaxyx screen))
  (define height (list-ref size 0))
  (define width (list-ref size 1))
  (define title-height 2)
  (define prompt-height 2)
  (set! title-win (newwin 2 width 0 0))
  (set! prompt-win
        (newwin prompt-height width (- height prompt-height) 0))
  (set! log-win
        (newwin (- height title-height prompt-height)
                width title-height 0))
  (scrollok! log-win #t)   ; scroll on text going beyond chat history

  ;; Setup OCapN
  (define hub-uri
    (string-trim-both (call-with-input-file client-sturdyref-file read-string)))
  (define hub-sref (string->ocapn-id hub-uri))
  (set! tcp-netlayer
        (with-vat machine-vat
          (new-tcp-tls-netlayer "127.0.0.1"
                                #:key (load-tls-private-key tls-key-file-name)
                                #:cert (load-tls-certificate tls-cert-file-name))))
  (set! mycapn
        (with-vat machine-vat
          (spawn-mycapn tcp-netlayer)))

  ;; Setup game actors
  (set! hub-vow
        (with-vat machine-vat
          ($ mycapn 'enliven hub-sref)))
  (set! identity-vow
        (with-vat user-vat
          (<- hub-vow 'make-identity client-name)))
  (set! profile-vow
        (with-vat user-vat
          (let-on ((identity identity-vow))
            (<- hub-vow 'make-profile identity))))
  (set! petnames
        (with-vat user-vat
          (spawn ^petname-registry)))
  (set! entity-vow
        (with-vat user-vat
          (let-on ((profile profile-vow))
            (<- hub-vow 'make-entity profile))))
  (set! controller
        (with-vat user-vat
          (let-on ((hub hub-vow)
                   (entity entity-vow))
            (spawn ^controller hub entity))))
  (set! logger
        (with-vat ui-vat
          (spawn ^logger:curses log-win)))

  ;; Connect to the game world!
  (with-vat user-vat
    ;; Register a petname for ourselves.
    (let-on ((profile profile-vow))
      ;; Get our self-proposed name registered in the local cache
      (lookup-name-for-profile profile)
      ($ petnames 'register-petname client-name profile))
    ;; Setup event handler.
    (<-np entity-vow 'set-event-handler (spawn ^event-handler:fancy logger))
    ;; Enter initial room.
    (on (<- controller 'enter (<- hub-vow 'spawn-point))
        (lambda _
          (add-task! tasks refresh-title)
          (log "Welcome to Fantasary!")
          (log "Type /help for command information.")
          (look))
        #:catch log-error))

  ;; Setup UI
  (add-task! tasks refresh-prompt))

(define (do-command input)
  (match (call-with-input-string input parse-command)
    (('quit)
     (let ((halt? (make-condition)))
       (define (signal-halt . _)
         (signal-condition! halt?))
       (with-vat user-vat
         (on (<- controller 'quit) signal-halt #:catch signal-halt))
       ;; Attempt to get a response from the server before quitting,
       ;; but don't wait forever.
       (perform-operation
        (choice-operation (wait-operation halt?)
                          (sleep-operation 1)))
       (halt-event-loop)))
    (command
     (with-vat user-vat
       (match command
         (('help)
          (help))
         (('say message)
          (say message))
         (('me message)
          (emote message))
         (('look)
          (echo input)
          (look))
         (('go exit)
          (echo input)
          (go exit))
         (('run target args ...)
          (echo input)
          (run target args))
         (('name (? name? old) (? petname? new))
          (echo input)
          (register-petname old new))
         (_
          (echo input)
          (log (format #f "Unknown command: ~s" command))))))))

(define (handle-input screen char)
;;; Uncomment for debugging input.
  ;; (addstr title-win (format #f "INPUT: ~s" char)
  ;;         #:x 0 #:y 0)
  ;; (refresh title-win)
  (cond
   ;; Exit
   ((or (eqv? char #\esc) (eqv? char #\etx))
    (halt-event-loop))
   ;; Backspace
   ((or (eqv? char %BACKSPACE) (eqv? char #\delete))
    (set! prompt-input (match prompt-input
                         (() '())
                         ((_ chars ...) chars))))
   ;; Submit command
   ((eqv? char #\newline)
    (let ((input (string-trim-both (list->string (reverse prompt-input)))))
      (unless (string-null? input)
        (do-command input))
      (set! prompt-input '())))
   ;; Add a char to the command line
   ((and (char? char)            ; some "characters" might be integers
         (char-set-contains? char-set:printing char))
    (set! prompt-input (cons char prompt-input))))
  (refresh-prompt))

(define (run-client our-name sturdyref-file)
  (set! client-name our-name)
  (set! client-sturdyref-file sturdyref-file)
  (generate-tls-cert-maybe! tls-key-file-name tls-cert-file-name)
  (run-event-loop
   #:init (trampoline init screen)
   #:handle-input (trampoline handle-input screen char)
   #:tasks tasks
   #:repl (false-if-exception (spawn-coop-repl-server))
   #:screen screen))

(define (launch-client args)
  (match args
    (()
     (run-client "NamelessGoblin"
                 (string-append %datadir "/sturdyref")))
    ((our-name)
     (run-client our-name
                 (string-append %datadir "/sturdyref")))
    ((our-name sturdyref-file)
     (run-client our-name sturdyref-file))
    (_
     (format #t "Usage: fantasary NAME [STURDYREF_FILE]\n")
     (exit 1))))
