;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (fantasary server)
  #:use-module (fantasary rooms)
  #:use-module (fantasary tls-cert)
  #:use-module (fibers conditions)
  #:use-module (fibers operations)
  #:use-module (fibers timers)
  #:use-module (goblins)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib let-on)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer tcp-tls)
  #:use-module (goblins vat)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (system repl server)
  #:use-module (system repl coop-server)
  #:export (launch-server))

(define tls-key-file-name "server.key")
(define tls-cert-file-name "server.cert")
(define machine-vat #f)
(define hub-vat #f)
(define hub #f)
(define repl #f)

(define-record-type <room>
  (make-room identity profile room)
  room?
  (identity room-identity)
  (profile room-profile)
  (room room-ref))

(define *rooms* (make-hash-table))

(define (lookup-room name)
  (hashq-ref *rooms* name))

(define (add-room! name room)
  (hashq-set! *rooms* name room))

(define (spawn-room! scheme-name world-name)
  (with-vat hub-vat
    (let* ((identity (spawn ^identity world-name))
           (profile (spawn ^profile identity))
           (room (spawn ^room profile))
           (room* (make-room identity profile room)))
      (add-room! scheme-name room*)
      room)))

(define-record-type <bot>
  (make-bot identity profile entity controller)
  bot?
  (identity bot-identity)
  (profile bot-profile)
  (entity bot-entity)
  (controller bot-controller))

(define *bots* (make-hash-table))

(define (lookup-bot name)
  (hashq-ref *bots* name))

(define (add-bot! name bot)
  (hashq-set! *bots* name bot))

(define (%set-bot-event-handler! bot ^event-handler)
  (let* ((profile (bot-profile bot))
           (entity (bot-entity bot))
           (controller (bot-controller bot)))
      ($ entity 'set-event-handler (spawn ^event-handler profile controller))))

(define (set-bot-event-handler! bot ^event-handler)
  (with-vat hub-vat
    (%set-bot-event-handler! bot ^event-handler)))

(define (spawn-bot! scheme-name world-name ^event-handler hub room)
  (with-vat hub-vat
    (let* ((identity (spawn ^identity world-name))
           (profile (spawn ^profile identity))
           (entity (spawn ^entity profile))
           (controller (spawn ^controller hub entity))
           (bot (make-bot identity profile entity controller)))
      (%set-bot-event-handler! bot ^event-handler)
      (<-np controller 'enter room)
      (add-bot! scheme-name bot))))

(define (sleep-vow seconds)
  (spawn-fibrous-vow
   (lambda ()
     (perform-operation
      (sleep-operation seconds))
     #t)))

(define (^greeter _bcom _profile controller)
  (define seen (spawn ^cell '()))
  (match-lambda
    (('enter who)
     (let-on ((profile (<- who 'profile)))
       (unless (memq profile ($ seen))
         ($ seen (cons profile ($ seen)))
         (let-on ((_wait (sleep-vow 3)))
           (<-np controller 'say `("Hey, " ,profile "! Haven't seen you here before."))
           (<-np controller 'say "Try typing: /run ?Greeter look")))))
    (('direct from 'look)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'emote `("looks at " ,from-profile " and nods approvingly."))
       (<-np controller 'say "Now try: /run ?Greeter talk")))
    (('direct from 'talk)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'emote `("gives " ,from-profile " a big thumbs up."))
       (<-np controller 'say "Nice job!  Bots commonly respond to 'look' or 'talk'.")
       (<-np controller 'say "Try those commands out on other bots you see around.")
       (<-np controller 'say "One last tip: You can give me a different name that only you can see!")
       (<-np controller 'say "Try this: /name ?Greeter @NiceBot")))
    (('direct from args ...)
     (<-np controller 'emote "looks puzzled."))
    (_ #f)))

(define (^poll _bcom _profile controller)
  (define voted (spawn ^cell '()))
  (define options '(emacs vim))
  (define tally (spawn ^cell '()))
  (define (count option)
    (fold (lambda (x sum)
            (if (eq? x option)
                (+ sum 1)
                sum))
          0 ($ tally)))
  (define (say-current-status)
    (<-np controller 'say "The current poll results are as follows:")
    (for-each (lambda (option)
                (<-np controller 'say (list option ": " (count option))))
              options)
    (<-np controller 'say "Cast your *vote* if you haven't yet!"))
  (match-lambda
    (('direct from 'look)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'emote `(,from-profile " inspects the tally sheet."))
       (say-current-status)))
    (('direct from 'vote option)
     (if (memq option options)
         (let-on ((from-profile (<- from 'profile)))
           (if (memq from-profile ($ voted))
               (<-np controller 'say "Reminder: You can only vote /once/.")
               (begin
                 ($ tally (cons option ($ tally)))
                 ($ voted (cons from-profile ($ voted)))
                 (say-current-status))))
         (begin
           (<-np controller 'say "Someone tried to vote for an invalid option!")
           (<-np controller 'say `("Valid options are: " ,(format #f "~s" options))))))
    (_ #f)))

(define (^dice-roller _bcom _profile controller)
  (match-lambda
    (('direct from 'look)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'say `("Want to *roll* the dice, " ,from-profile "?"))))
    (('direct from (or 'roll 'roll-dice))
     (let-on ((from-profile (<- from 'profile)))
       (let ((r1 (+ (random 6) 1))
             (r2 (+ (random 6) 1)))
         (<-np controller 'emote "rolls the dice.")
         (<-np controller 'say `(,from-profile " rolled a " ,r1 " and a " ,r2 ".")))))
    (('direct from args ...)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'say `("I don't know that command, " ,from-profile ". Try 'roll'."))))
    (_ #f)))

(define (^barista _bcom profile controller)
  (define busy? (spawn ^cell #f))
  (define (say-busy)
    (<-np controller 'say "I'm busy right now. Check back when I'm done!"))
  (match-lambda
    (('direct from 'talk)
     (if ($ busy?)
         (say-busy)
         (let-on ((from-profile (<- from 'profile)))
           (<-np controller 'emote
                 (list "notices " from-profile " approach the counter."))
           (<-np controller 'say
                 (list "Hello, " from-profile ". What would you like to order today?"))
           (<-np controller 'say
                 (list "We have coffee or tea.  We're a simple shop.")))))
    (('direct from 'order item)
     (let-on ((from-profile (<- from 'profile)))
       (let ((name (match item
                     ((or 'coffee 'Coffee 'COFFEE "coffee" "Coffee" "COFFEE")
                      "coffee")
                     ((or 'tea 'Tea 'TEA "tea" "Tea" "TEA")
                      "tea")
                     (_ #f))))
         (cond
          (($ busy?)
           (say-busy))
          (name
           (begin
             ($ busy? #t)
             (<-np controller 'say
                   (list "One " name " for " from-profile " coming right up!"))
             (<-np controller 'emote "gets to work")
             (let-on ((_wait (sleep-vow 5)))
               (<-np controller 'emote
                     (list "hands " from-profile " a piping hot cup of " name "."))
               (<-np controller 'say "Too bad we don't have inventory implemented or you could take that with you.")
               ($ busy? #f))))
          (else
           (<-np controller 'say (list "Sorry " from-profile ", we don't have that.")))))))
    (_ #f)))

(define (^like-accumulator _bcom _profile controller)
  (define likes (spawn ^cell 0))
  (match-lambda
    (('direct from 'look)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'emote `("notices " ,from-profile "."))
       (<-np controller 'say `("Hi, " ,from-profile "! Make sure to smash that *like* button!"))))
    (('direct from 'like)
     (let-on ((from-profile (<- from 'profile)))
       ($ likes (+ ($ likes) 1))
       (<-np controller 'emote `("is liked by " ,from-profile "."))
       (<-np controller 'say `("I have accumulated " ,($ likes) " likes!"))))
    (('direct from args ...)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'say `("I don't know that command, " ,from-profile "."))))
    (_ #f)))

(define (^radio _bcom _profile controller)
  (define stations
    '(("88.7" "The Goblins News Network" "Your source for the latest asynchronous news.")
      ("94.5" "The Jam Spot" "The hottest new hip hop this side of the wire!")
      ("102.1" "Back in the Day Radio" "Classic rock up and down the block.")
      ("103.3" "Amped Up Radio" "Top 40 beeps *and* boops!")
      ("107.3" "Metal Gods" "The only station that *really* rocks!")))
  (define n (length stations))
  (define index (spawn ^cell 0))
  (define (say-current-station)
    (match (list-ref stations ($ index))
      ((frequency name description)
       (<-np controller 'say `("You're listening to " ,frequency ": " ,name "."))
       (<-np controller 'say description))))
  (match-lambda
    (('direct from 'look)
     (<-np controller 'emote "buzzes with static")
     (<-np controller 'say "Don't *touch* that dial!")
     (say-current-station))
    (('direct from 'touch)
     (let-on ((from-profile (<- from 'profile)))
       ($ index (modulo (+ ($ index) 1) n))
       (<- controller 'emote `("has its station changed by " ,from-profile "."))
       (say-current-station)))
    (('direct from args ...)
     (let-on ((from-profile (<- from 'profile)))
       (<-np controller 'say `("I don't know that command, " ,from-profile "."))))
    (_ #f)))

(define (run-server host)
  (format #t "Generating TLS certs (if not present)\n")
  (generate-tls-cert-maybe! tls-key-file-name tls-cert-file-name)
  (format #t "Spawning REPL server\n")
  (set! repl (spawn-coop-repl-server (make-tcp-server-socket #:port 37147)))
  (format #t "Spawning vats\n")
  (set! machine-vat (spawn-vat #:name "OCapN" #:log? #t))
  (set! hub-vat (spawn-vat #:name "Hub" #:log? #t))
  (format #t "Spawning rooms\n")
  (define lobby (spawn-room! 'lobby "Lobby"))
  (define cafe (spawn-room! 'cafe "Cafe"))
  (define arcade (spawn-room! 'Arcade "Arcade"))
  (with-vat hub-vat
    ($ lobby 'add-exit cafe)
    ($ lobby 'add-exit arcade)
    ($ cafe 'add-exit lobby)
    ($ arcade 'add-exit lobby))
  (format #t "Spawning hub\n")
  (set! hub (with-vat hub-vat (spawn ^hub lobby)))
  (format #t "Spawning bots\n")
  (spawn-bot! 'greeter "Greeter" ^greeter hub lobby)
  (spawn-bot! 'radio "Radio" ^radio hub lobby)
  (spawn-bot! 'barista "Barista" ^barista hub cafe)
  (spawn-bot! 'dice-roller "DiceRoller" ^dice-roller hub arcade)
  (spawn-bot! 'like-bot "LikeBot" ^like-accumulator hub arcade)
  (spawn-bot! 'poll "Poll" ^poll hub arcade)
  (format #t "Spawning OCapN\n")
  (define tcp-netlayer
    (with-vat machine-vat
      (new-tcp-tls-netlayer host
                            #:key (load-tls-private-key tls-key-file-name)
                            #:cert (load-tls-certificate tls-cert-file-name))))
  (define mycapn
    (with-vat machine-vat
      (spawn-mycapn tcp-netlayer)))
  ;; Hardcoded swissnum so we can restart the server and get the same
  ;; sturdyref every time.
  (define hub-swissnum
    #vu8(122 117 170 244 60 243 73 17 188 154 83 45 102 130 160 166
             149 75 122 144 222 121 143 80 181 76 90 76 249 73 228 201))
  (format #t "Registering hub with OCapN\n")
  (define hub-sref
    (with-vat machine-vat
      (define registry ($ mycapn 'get-registry))
      (define location ($ tcp-netlayer 'our-location))
      ($ registry 'register hub hub-swissnum)
      (make-ocapn-sturdyref location hub-swissnum)))
  (format #t "Fantasary is now running!\n")
  (format #t "Connect clients to:\n~a\n" (ocapn-id->string hub-sref))
  (call-with-output-file "sturdyref"
    (lambda (port)
      (display (ocapn-id->string hub-sref) port)))
  (while #t
    (usleep 10000)
    (poll-coop-repl-server repl)))

(define (launch-server args)
  (match args
    (()
     (run-server "localhost"))
    ((host)
     (run-server host))
    (_
     (format #t "Usage: fantasary-server [HOSTNAME]\n")
     (exit 1))))
