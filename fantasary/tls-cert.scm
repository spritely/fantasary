;;; Copyright 2023 David Thompson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (fantasary tls-cert)
  #:use-module (fantasary config)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:export (generate-tls-cert!
            generate-tls-cert-maybe!))

(define (generate-tls-cert! key-file-name cert-file-name)
  (parameterize ((current-error-port (%make-void-port "w")))
    (unless (zero?
             (system* openssl "req"
                      "-new"
                      "-newkey" "rsa:4096"
                      "-days" "365"
                      "-nodes"
                      "-x509"
                      ;; This info doesn't matter since we aren't
                      ;; dealing with certificate authorities.  We
                      ;; just need openssl to not prompt for user
                      ;; input so that this is fully automated.
                      "-subj" "/C=US/ST=OfMind/L=Interweb/O=Spritely/CN=www.example.com"
                      "-keyout" key-file-name
                      "-out" cert-file-name))
      (throw 'tls-error "failed to generate TLS certificate"))))

(define (generate-tls-cert-maybe! key-file-name cert-file-name)
  (unless (and (file-exists? key-file-name)
               (file-exists? cert-file-name))
    (generate-tls-cert! key-file-name cert-file-name)))
