(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix download)
             (guix git)
             (guix git-download)
             (guix build-system gnu)
             (guix utils)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages tls))

(define guile-goblins*
  (let ((commit "24b323f31450461dbeed6edbb705e1b400d86fce"))
    (package
      (inherit guile-goblins)
      (version (string-append (package-version guile-goblins)
                              "-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/spritely/guile-goblins.git")
                      (commit commit)))
                (sha256
                 (base32
                  "058qavcb7pyg0dbdcizl2kqczb851mjh9pfgl4mjf6fyzxbfy985"))))
      (native-inputs
       (list autoconf automake pkg-config texinfo))
      (inputs (list guile-3.0))
      (propagated-inputs
       (list guile-fibers guile-gcrypt guile-gnutls)))))

(package
  (name "fantasary")
  (version "0.1.0-git")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system gnu-build-system)
  (arguments
   `(#:make-flags '("GUILE_AUTO_COMPILE=0")
     #:modules ((ice-9 match) (ice-9 ftw)
                ,@%gnu-build-system-modules)
     #:phases
     (modify-phases %standard-phases
       ;; Wrap executables so they have the right guile load path.
       (add-after 'install 'wrap-fantasary
         (lambda* (#:key inputs outputs #:allow-other-keys)
           (let* ((out  (assoc-ref outputs "out"))
                  (bin  (string-append out "/bin"))
                  (site (string-append out "/share/guile/site"))
                  (deps (list (assoc-ref inputs "guile-gcrypt")
                              (assoc-ref inputs "guile-fibers")
                              (assoc-ref inputs "guile-goblins")
                              (assoc-ref inputs "guile-ncurses")
                              (assoc-ref inputs "guile-gnutls"))))
             (match (scandir site)
               (("." ".." version)
                (let ((modules (string-append site "/" version))
                      (compiled-modules (string-append
                                         out "/lib/guile/" version
                                         "/site-ccache")))
                  (for-each (lambda (program)
                              (wrap-program (string-append bin "/" program)
                                `("GUILE_LOAD_PATH" ":" prefix
                                  (,modules
                                   ,@(map (lambda (dep)
                                            (string-append dep
                                                           "/share/guile/site/"
                                                           version))
                                          deps)))
                                `("GUILE_LOAD_COMPILED_PATH" ":" prefix
                                  (,compiled-modules
                                   ,@(map (lambda (dep)
                                            (string-append dep "/lib/guile/"
                                                           version
                                                           "/site-ccache"))
                                          deps)))))
                            '("fantasary" "fantasary-server"))
                  #t)))))))))
  (native-inputs
   (list autoconf automake pkg-config))
  (inputs
   (list guile-3.0 openssl))
  (propagated-inputs
   (list guile-goblins* guile-ncurses))
  (synopsis "Text-based virtual world")
  (description "Fantasary is a text-based virtual world created for the Spring Lisp
Game Jam 2023.")
  (home-page "https://spritely.institute")
  (license license:asl2.0))
