(use-modules (fantasary concurrent-queue)
             (fantasary event-loop)
             (fantasary ncurses-vat)
             (goblins)
             (goblins vat)
             (ncurses curses))

(define tasks (make-concurrent-queue))
(define vat (make-ncurses-vat tasks))

(define (^greeter _bcom our-name)
  (lambda (your-name)
    (format #f "Hello ~a, my name is ~a!" your-name our-name)))

(define (init screen)
  (vat-start! vat)
  (with-vat vat
    (define alice (spawn ^greeter "Alice"))
    (addstr screen ($ alice "Dave"))))

(define (handle-input screen char)
  (when (or (eqv? char #\esc) (eqv? char #\etx))
    (halt-event-loop)))

(run-event-loop
 #:init init
 #:handle-input handle-input
 #:tasks tasks)
