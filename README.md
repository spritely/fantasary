# Fantasary

Fantasary is a text-based virtual world created for the [Spring Lisp
Game Jam 2023](https://itch.io/jam/spring-lisp-game-jam-2023).
Fantasary is written in [Guile
Scheme](https://www.gnu.org/software/guile/) using the [Spritely
Goblins](https://spritely.institute/goblins/) distributed object
programming environment.

## Building from source

You will need:
* [GNU Guile](https://www.gnu.org/software/guile/) >= 3.0.0
* [guile-ncurses](https://www.gnu.org/software/guile-ncurses/) >= 3.0
* [Spritely Goblins](https://spritely.institute/goblins/) >= 0.11.0
* [Fibers](https://github.com/wingo/fibers) >= 1.0.0 (Goblins dependency)
* [guile-gcrypt](https://notabug.org/cwebber/guile-gcrypt/) >= 0.4.0 (Goblins dependency)
* [OpenSSL](https://www.openssl.org/) (the `openssl` executable is
  used to generate a TLS certificate)

The easiest way to get all of the dependencies is to install the [Guix
package manager](https://guix.gnu.org/) and run `guix shell` from the
directory that this README is in.

Once all dependencies are installed, build like so:

```
./bootstrap.sh
./configure
make -j$(nproc)
```

## Running the client

To run the game from the source checkout, run:

```
./pre-inst-env fantasary [NAME] [STURDYREF_FILE] [PORT]
```

If `NAME` is not specified, you will appear as a nameless goblin.  The
`STURDYREF_FILE` argument is not necessary unless you want to connect
to a custom server rather than the default one run by the Spritely
Institute.  `PORT` is almost certainly unnecessary, unless another
service is running on port 8676 on your machine.  If you'd like to
start more than one client per machine, specify a different port.

## Running the server

To run the server from the source checkout, run:

```
./pre-inst-env fantasary-server [PORT]
```

The port is optional and defaults to 8675.

## Deploying

Generate a relocatable Guix pack like so:

```
guix pack -R -m pack.scm -S /bin=bin
```
