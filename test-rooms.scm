(use-modules (fantasary concurrent-queue)
             (fantasary event-loop)
             (fantasary ncurses-vat)
             (fantasary ncurses-stuff)
             (fantasary rooms)
             (goblins)
             (goblins actor-lib joiners)
             (goblins actor-lib let-on)
             (goblins vat)
             (ice-9 match)
             (ice-9 rdelim)
             (ncurses curses)
             (srfi srfi-1)
             (system repl coop-server))

(define tasks (make-concurrent-queue))
(define room-vat (spawn-vat #:name "Hub" #:log? #t))
(define a-vat (spawn-vat #:name "Alice" #:log? #t))
(define b-vat (spawn-vat #:name "Bob" #:log? #t))
(define n-vat (make-ncurses-vat tasks))
(define hub #f)
(define lobby-identity #f)
(define lobby #f)
(define cafe-identity #f)
(define cafe #f)
(define registrar #f)
(define logger #f)
(define alice-vow #f)
(define a-identity-vow #f)
(define a-profile-vow #f)
(define a-handler #f)
(define a-controller #f)
(define a-petnames #f)
(define bob-vow #f)
(define b-identity-vow #f)
(define b-profile-vow #f)
(define b-handler #f)
(define b-controller #f)
(define title-win #f)
(define log-win #f)
(define prompt-win #f)
(define prompt-input '())
(define screen (screen-setup!))

(define (^bob-bot _bcom controller)
  (match-lambda
    (('direct from 'roll-dice)
     (let-on ((from-profile (<- from 'profile)))
       (let ((roll (+ (random 6) 1)))
         (<-np controller 'emote "rolls a die")
         (<-np controller 'say (list from-profile " rolled a " roll)))))
    (('direct args ...)
     (<-np controller 'say (format #f "unknown command: ~s" args)))
    (_ #f)))

(define (^logger _bcom win)
  (lambda (text)
    (addstr win text)
    (addstr win "\n")
    (refresh win)
    (refresh prompt-win)))   ; move cursor back to prompt

(define (all-of*-maybe refrs)
  (match refrs
    (()
     (let ()
       (define-values (promise resolver)
         (spawn-promise-values))
       ($ resolver 'fulfill '())
       promise))
    (_
     (all-of* refrs))))

(define (^test-handler _bcom logger petnames)
  (define (log message)
    (<-np logger message))
  ;; Transform a message expression containing strings, numbers, and
  ;; profiles into a human readable string for printing.  Profiles are
  ;; replaced with petnames (if defined) or self-proposed names.
  (define (render-message message name-map)
    (match message
      ((? string?)
       message)
      ((? number? n)
       (number->string n))
      ((? live-refr? obj)
       (or (assq-ref name-map obj)
           (format #f "~s" obj)))
      ((items ...)
       (string-concatenate
        (map (lambda (item)
               (render-message item name-map))
             items)))
      (_ (format #f "~a" message))))
  ;; Find all refrs (assumed to be profiles later) in a message
  ;; expression.
  (define (find-refrs exp)
    (match exp
      ((? live-refr? refr)
       (list refr))
      ((exps ...)
       (delete-duplicates
        (concatenate
         (map find-refrs exps))))
      (_ '())))
  (define (find-promises exp)
    (match exp
      ((? promise-refr? refr)
       (list refr))
      ((exps ...)
       (delete-duplicates
        (concatenate
         (map find-promises exps))))
      (_ '())))
  ;; Resolve all promises in message so that we have actual profile
  ;; references that we can lookup petnames for.
  (define (resolve-message exp)
    (let ((promises (find-promises exp)))
      (let-on ((resolved (all-of*-maybe promises)))
        (let ((table (map cons promises resolved)))
          (let loop ((exp exp))
            (match exp
              ((? promise-refr? refr)
               (assq-ref table refr))
              ((exps ...)
               (map loop exps))
              (_ exp)))))))
  ;; Build an alist mapping profiles to names (petnames or
  ;; self-proposed).
  (define (make-name-map profiles)
    (let-on ((names (all-of*-maybe
                     (map (lambda (profile)
                            (lookup-name-for-profile petnames profile))
                          profiles))))
      (map cons profiles names)))
  (match-lambda
    (('enter who)
     (on (lookup-name-for-entity petnames who)
         (lambda (name)
           (log (format #f "*** ~a has joined" name)))))
    (('exit who)
     (on (lookup-name-for-entity petnames who)
         (lambda (name)
           (log (format #f "*** ~a has left" name)))))
    (('say who message)
     (let*-on ((message* (resolve-message message))
               (profiles (all-of*-maybe (find-refrs message*))))
       (let-on ((name (lookup-name-for-entity petnames who))
                (name-map (make-name-map profiles)))
         (log (format #f "<~a> ~a" name (render-message message* name-map))))))
    (('emote who message)
     (let*-on ((message* (resolve-message message))
               (profiles (all-of*-maybe (find-refrs message*))))
       (let-on ((name (lookup-name-for-entity petnames who))
                (name-map (make-name-map profiles)))
         (log (format #f "* ~a ~a" name (render-message message* name-map))))))
    (event                              ; unknown event
     (log (format #f "~s" event)))))

(define (lookup-name-for-profile petnames profile)
  (let-on ((petname (<- petnames 'lookup-petname profile))
           (self-proposed-name (<- profile 'self-proposed-name)))
    (if petname
        (string-append "@" petname)
        (string-append "?" self-proposed-name))))

(define (lookup-name-for-entity petnames entity)
   (let-on ((profile (<- entity 'profile)))
     (lookup-name-for-profile petnames profile)))

(define (lookup-name-for-room petnames room)
   (let-on ((profile (<- room 'profile)))
     (lookup-name-for-profile petnames profile)))

(define (refresh-prompt screen)
  (define size (getmaxyx screen))
  (define width (list-ref size 1))
  (define prompt-str (list->string (reverse prompt-input)))
  (define prompt-str-len (string-length prompt-str))
  (define max-prompt-to-show (- width 4))
  (clear prompt-win)
  (hline prompt-win (acs-hline) width #:x 0 #:y 0)
  (addch prompt-win
         (color %YELLOW-N (bold #\>))
         #:x 1 #:y 1)
  (addstr prompt-win
          ;; cut off the length of the string shown, if the user has typed a lot
          (if (> prompt-str-len max-prompt-to-show)
              (let ((str-start (- prompt-str-len max-prompt-to-show)))
                (substring prompt-str str-start))
              prompt-str)
          #:x 3 #:y 1)
  (move prompt-win 1 (+ 3 (length prompt-input)))
  (refresh prompt-win))

(define (refresh-title)
  (define size (getmaxyx screen))
  (define width (list-ref size 1))
  (with-vat n-vat
    (let*-on ((profile (<- a-controller 'room-profile))
              (name (lookup-name-for-profile a-petnames profile)))
      (clear title-win)
      (addstr title-win (format #f "Room: ~a" name) #:x 0 #:y 0)
      (hline title-win (acs-hline) width #:x 0 #:y 1)
      (refresh title-win))))

(define (init screen)
  (vat-start! n-vat)
  ;; Setup ncurses windows
  (define size (getmaxyx screen))
  (define height (list-ref size 0))
  (define width (list-ref size 1))
  (define title-height 2)
  (define prompt-height 2)
  (set! title-win (newwin 2 width 0 0))
  (set! prompt-win
        (newwin prompt-height width (- height prompt-height) 0))
  (set! log-win
        (newwin (- height title-height prompt-height)
                width title-height 0))
  (scrollok! log-win #t)   ; scroll on text going beyond chat history
  ;; Setup rooms
  (set! lobby-identity (with-vat room-vat (spawn ^identity "Lobby")))
  (set! lobby (with-vat room-vat (spawn ^room (spawn ^profile lobby-identity))))
  (set! cafe-identity (with-vat room-vat (spawn ^identity "Cafe")))
  (set! cafe (with-vat room-vat (spawn ^room (spawn ^profile cafe-identity))))
  ;; Setup identity hub
  (set! hub (with-vat room-vat (spawn ^hub lobby)))
  ;; Setup Alice
  (set! logger (with-vat n-vat (spawn ^logger log-win)))
  (set! a-petnames (with-vat a-vat (spawn ^petname-registry)))
  (set! a-handler (with-vat a-vat (spawn ^test-handler logger a-petnames)))
  (set! a-identity-vow
        (with-vat a-vat
          (<- hub 'make-identity "Alice")))
  (set! a-profile-vow
        (with-vat a-vat
          (let-on ((identity a-identity-vow))
            (<- hub 'make-profile identity))))
  (set! alice-vow
        (with-vat a-vat
          (let-on ((profile a-profile-vow))
            (spawn ^entity profile a-handler))))
  (set! a-controller (with-vat a-vat (spawn ^controller alice-vow)))
  ;; Setup Bob
  (set! b-identity-vow
        (with-vat b-vat
          (<- hub 'make-identity "Bob")))
  (set! b-profile-vow
        (with-vat b-vat
          (let-on ((identity b-identity-vow))
            (<- hub 'make-profile identity))))
  (set! bob-vow (with-vat b-vat
                  (let-on ((profile b-profile-vow))
                    (spawn ^entity profile))))
  (set! b-controller (with-vat b-vat (spawn ^controller bob-vow)))
  (set! b-handler (with-vat b-vat (spawn ^bob-bot b-controller)))
  ;; Add exits to rooms.
  (with-vat room-vat
    (<-np lobby 'add-exit cafe)
    (<-np cafe 'add-exit lobby))
  ;; Register Alice as a petname, since she knows herself.
  (with-vat a-vat
    (let-on ((profile a-profile-vow))
      (<-np a-petnames 'register "Alice" profile)))
  ;; Setup Bob's bot event handler
  (with-vat b-vat (<-np bob-vow 'set-event-handler b-handler))
  ;; Put Alice and Bob into the lobby.
  (with-vat b-vat (<-np b-controller 'enter lobby))
  (with-vat a-vat (<-np a-controller 'enter lobby))
  ;; Perform an initial look
  (with-vat a-vat (do-look))
  ;; Render room name
  (refresh-title)
  ;; Initialize input prompt.
  (add-task! tasks (lambda () (refresh-prompt screen))))

;; Curses command-line parser.
(define (parse-command port)
  (define (whitespace? char)
    (char-set-contains? char-set:whitespace char))
  (define (name-prefix? char)
    (or (eqv? char #\?)
        (eqv? char #\@)))
  (define (alphanumeric? char)
    (char-set-contains? char-set:letter+digit char))
  (define (consume-whitespace)
    (match (peek-char port)
      ((? eof-object?) *unspecified*)
      ((? whitespace?)
       (read-char port)
       (consume-whitespace))
      (_ *unspecified*)))
  (define (parse-verb)
    (match (peek-char port)
      (#\/
       (read-char port)
       (string->symbol
        (list->string
         (let loop ()
           (match (read-char port)
             ((or (? eof-object?) (? whitespace?))
              '())
             (char
              (cons char (loop))))))))
      (_ 'say)))
  (define (parse-plain-text)
    (list->string
     (let loop ((prev-whitespace? #f))
       (match (peek-char port)
         ((? eof-object?)
          '())
         ((? whitespace? char)
          (read-char port)
          (cons char (loop #t)))
         ((and (? name-prefix?) char)
          (if prev-whitespace?
              '()
              (begin
                (read-char port)
                (cons char (loop #f)))))
         (char
          (read-char port)
          (cons char (loop #f)))))))
  ;; TODO: Support some type of additional notation to read names with
  ;; non-alphanumeric characters. Maybe "@{Foo Bar}" or something?
  (define (parse-name)
    (define type
      (match (read-char port)
        (#\? 'self-proposed-name)
        (#\@ 'petname)))
    (list type
          (list->string
           (let loop ()
             (match (peek-char port)
               ((? eof-object?) '())
               ((? alphanumeric?)
                (cons (read-char port) (loop)))
               (_ '()))))))
  (define (parse-markup)
    (match (peek-char port)
      ((? eof-object?)
       '())
      ((? name-prefix?)
       (cons (parse-name) (parse-markup)))
      (_
       (cons (parse-plain-text) (parse-markup)))))
  (define (parse-run-arg arg)
    (match arg
      ((or ('self-proposed-name _) ('petname _))
       (list arg))
      (_
       (call-with-input-string arg
         (lambda (port*)
           (let loop ()
             (match (read port*)
               ((? eof-object?)
                '())
               (exp
                (cons exp (loop))))))))))
  (define (parse-scheme)
    (match (read port)
      ((? eof-object?)
       '())
      (exp
       (cons exp (parse-scheme)))))
  (define (expand-names exp)
    (match exp
      ((? symbol?)
       (let ((str (symbol->string exp)))
         (cond
          ((eqv? (string-ref str 0) #\?)
           `(self-proposed-name
             ,(string-drop str 1)))
          ((eqv? (string-ref str 0) #\@)
           `(petname
             ,(string-drop str 1)))
          (else exp))))
      ((exps ...)
       (map expand-names exps))
      (_ exp)))
  (consume-whitespace)
  (define verb (parse-verb))
  (define args
    (if (memq verb '(say me))
        (list (parse-markup))
        (expand-names (parse-scheme))))
  (cons verb args))

(define (resolve-names petnames controller exp)
  (match exp
    (('self-proposed-name name)
     (let-on ((entity (<- controller 'lookup-by-name name 0))
              (room (<- controller 'lookup-exit-by-name name 0)))
       (cond
        (entity (<- entity 'profile))
        (room (<- room 'profile))
        (else #f))))
    (('petname name)
     (<- petnames 'lookup-profile name))
    ((exps ...)
     (map (lambda (exp*)
            (resolve-names petnames controller exp*))
          exps))
    (_ exp)))

(define (echo message)
  (<-np logger (string-append "> " message)))

(define (log message)
  (<-np logger message))

(define (do-look)
  (let-on ((contents (<- a-controller 'look)))
    (let ((entities (assq-ref contents 'entities))
          (exits (assq-ref contents 'exits)))
      (let-on ((entity-names (all-of*-maybe
                              (map (lambda (e)
                                     (lookup-name-for-entity a-petnames e))
                                   entities)))
               (exit-names (all-of*-maybe
                            (map (lambda (room)
                                   (lookup-name-for-room a-petnames room))
                                 exits))))
        (match entity-names
          (()
           (log "There's nothing else in this room.  Just you."))
          (_
           (log "You look around the room and see:")
           (for-each (lambda (name)
                       (log (format #f "- ~a" name)))
                     entity-names)))
        (match exit-names
          (()
           (log "Uh oh, there are no exits!"))
          (_
           (log "You see some exits:")
           (for-each (lambda (name)
                       (log (format #f "- ~a" name)))
                     exit-names)))))))

(define (do-command input)
  (match (call-with-input-string input parse-command)
    (('quit)
     (halt-event-loop))
    (command
     (with-vat a-vat
       (let ((command* (resolve-names a-petnames a-controller command)))
         (match command*
           ;; Talk
           (('say message)
            (<-np a-controller 'say message))
           ;; Emote
           (('me message)
            (<-np a-controller 'emote message))
           ;; Describe room
           (('look)
            (echo input)
            (do-look))
           ;; Enter a new room
           (('go target)
            (echo input)
            (if (live-refr? target)
                (let*-on ((target* target) ; resolve promise
                          (room (<- a-controller 'lookup-exit-by-profile target*)))
                  (if room
                      (on (<- a-controller 'enter room)
                          (lambda _
                            (add-task! tasks refresh-title)
                            (let-on ((room-name (lookup-name-for-room a-petnames room)))
                              (log (format #f "You have entered ~a." room-name))
                              (do-look)))
                          #:catch
                          (lambda _
                            (let-on ((room-name (lookup-name-for-room a-petnames room)))
                              (log (format #f "Could not enter ~a." room-name)))))
                      (log "Invalid room name")))
                (log "Invalid room name")))
           ;; Register petname
           (('name (= symbol->string self-proposed-name)
                   (= symbol->string petname))
            (echo input)
            (let-on ((entity (<- a-controller 'lookup-by-name self-proposed-name 0))
                     (exit (<- a-controller 'lookup-exit-by-name self-proposed-name 0))
                     (current-room-name (<- a-controller 'room-name)))
              (let ((profile-vow (cond
                                  ((string=? self-proposed-name current-room-name)
                                   (<- a-controller 'room-profile))
                                  (entity
                                   (<- entity 'profile))
                                  (exit
                                   (<- exit 'profile))
                                  (else #f))))
                (if profile-vow
                    (let-on ((profile profile-vow))
                      ($ a-petnames 'register petname profile)
                      (log (format #f "Registered petname @~a for ?~a"
                                   petname self-proposed-name))
                      (add-task! tasks refresh-title))
                    (log (format #f "No object with self-proposed name: ~a"
                                 self-proposed-name))))))
           (('petnames)
            (let-on ((debug-data (<- a-petnames 'debug)))
              (log (format #f "~s" debug-data))))
           ;; Send command to entity
           (('run target args ...)
            (echo input)
            (let-on ((target* target))
              (if target*
                  (let-on ((entity (<- a-controller 'lookup-by-profile target*)))
                    (<-np entity 'notify args))
                  (log "No such user"))))
           (_
            (log (format #f "Unknown command: ~s" command*)))))))))

(define %BACKSPACE 263)

(define (handle-input screen char)
  ;;; Uncomment for debugging input.
  ;; (addstr title-win (format #f "INPUT: ~s" char)
  ;;         #:x 0 #:y 0)
  ;; (refresh title-win)
  (cond
   ;; Exit
   ((or (eqv? char #\esc) (eqv? char #\etx))
    (halt-event-loop))
   ;; Backspace
   ((or (eqv? char %BACKSPACE) (eqv? char #\delete))
    (set! prompt-input (match prompt-input
                         (() '())
                         ((_ chars ...) chars))))
   ;; Submit command
   ((eqv? char #\newline)
    (let ((input (string-trim-both (list->string (reverse prompt-input)))))
      (unless (string-null? input)
        (do-command input))
      (set! prompt-input '())))
   ;; Add a char to the command line
   ((and (char? char)   ; some "characters" might be integers
         (char-set-contains? char-set:printing char))
    (set! prompt-input (cons char prompt-input))))
  (refresh-prompt screen))

(define-syntax-rule (proxy proc args ...)
  (lambda (args ...)
    (proc args ...)))

(run-event-loop
 #:init (proxy init screen)
 #:handle-input (proxy handle-input screen char)
 #:tasks tasks
 #:repl (spawn-coop-repl-server)
 #:screen screen)
