(use-modules (guix packages)
             ((guix licenses) #:prefix license:)
             (guix download)
             (guix build-system trivial)
             (guix utils)
             (gnu packages)
             (gnu packages base)
             (gnu packages bash)
             (gnu packages guile)
             (gnu packages terminals))

(define fantasary
  (load (string-append (dirname (current-filename)) "/guix.scm")))

(define fantasary-wrapper
  (package
    (name "fantasary-wrapper")
    (version "0.1.0-git")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (bin (string-append out "/bin"))
                (cool-wrapper (string-append bin "/fantasary"))
                (uncool-wrapper (string-append bin "/fantasary-fallback"))
                (bash (search-input-file %build-inputs "/bin/bash"))
                (cool-retro-term (search-input-file %build-inputs "/bin/cool-retro-term"))
                (fantasary (search-input-file %build-inputs "/bin/fantasary"))
                (locales (string-append (assoc-ref %build-inputs "glibc-utf8-locales") "/lib/locale")))
           (mkdir-p bin)
           (call-with-output-file cool-wrapper
             (lambda (port)
               (format port "#!~a\n" bash)
               (format port "export GUIX_LOCPATH=~a\n" locales)
               (format port "exec ~a --profile Futuristic -e ~a \"$@\"\n" cool-retro-term fantasary)))
           (chmod cool-wrapper #o755)
           (call-with-output-file uncool-wrapper
             (lambda (port)
               (format port "#!~a\n" bash)
               (format port "export GUIX_LOCPATH=~a\n" locales)
               (format port "exec ~a \"$@\"\n" fantasary)))
           (chmod uncool-wrapper #o755)
           #t))))
    (inputs
     (list bash cool-retro-term fantasary glibc-utf8-locales))
    (synopsis "Wrapper for Fantasary client that launches in cool-retro-term.")
    (description "Fantasary is a text-based virtual world created for the Spring Lisp
Game Jam 2023.")
    (home-page "https://spritely.institute")
    (license license:asl2.0)))

(packages->manifest (list fantasary-wrapper))
